#!/bin/bash

if [ -z "$REBOOT_COUNT" ] || [ "$REBOOT_COUNT" -eq 0 ]; then
    tmt-reboot
fi
until [ "$output" == "No jobs running." ]; do
    output=$(systemctl list-jobs)
    sleep 1
done
echo -e "\n Reboot Complete"
