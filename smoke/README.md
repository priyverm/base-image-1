# Base image smoke tests

Simple test suite of smoke tests for the base RHIVOS image

## To run the smoke tests locally
`tmt run plans -n /smoke/plans/All -vvv`

## To run the smoke tests on provision virtual
`tmt run -a -vvv plans -n /smoke/plans/All provision --how virtual --image https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211119.0.x86_64.qcow2`

## To run the smoke tests on provision minute
`tmt run -a -vvv plans -n /smoke/plans/All provision --how minute --image 1MT-CentOS-Stream-9`

## To run specific plan for running tests for e.g. Testing Farm
`tmt run -a -vvv plans -n /smoke/plans/BasicImageValidation provision --how connect -u root -p password -g 1.2.3.4`

## To run specific set of tests inside smoke directory
Just change the name filter for plans to you desired test plan, e.g.:
`tmt run -a -vvv tests -n /smoke/tests/distro provision --how virtual --image https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20211119.0.x86_64.qcow2`

## This Test Suite includes multiple smoke tests

1. Boot tests - check that image is booted properly
2. Disk tests - partition type
3. Display tests - Xorg and neptune3-ui running
4. Distro tests - check if we running on CentOS Stream
5. DNF tests - check enabled repositories
6. Ostree tests - check if we are running Ostree image
7. Service tests - check default services are running

