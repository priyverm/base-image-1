#!/bin/bash

. $(dirname $(readlink --canonicalize $0))/setup.sh

printf "%s\n" "-- Running af_unix"
./tst_sys_af-unix >/dev/null &

printf "%s\n" "-- Running af_unix test in confusion"
podman run $PARAMS --name confusion $IMAGE ./tst_af-unix > /dev/null

cleanup

