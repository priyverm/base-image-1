#!/bin/bash

set -eE
cleanup() {
  podman rm -if orderly
  podman rm -if confusion
}

wait_for_fragmentation() {
  if test -n "$1"; then
    timeout="$1"
  else
    timeout=60
  fi

  seconds_start=$(date +%s)
  while true; do
    seconds_end=$(date +%s)
    elapsed=$(( seconds_end - seconds_start ))
    if podman logs --tail 1 confusion | grep -q "buddy system fragmented"; then
      echo "-- System memory fragmented after ${elapsed}s"
      return 0
    fi
    if test $elapsed -ge $timeout; then
      return 1
    fi
    sleep 1
  done
}

mem_test_size() {
  size_total_mb=$(grep "MemAvailable" /proc/meminfo | awk '{ print $2 }')
  echo "$size_total_mb / (2 * 1024) * (1 - 0.1)" | bc
}

mem_size_mb=$(mem_test_size)

chcon -R -t container_file_t .

trap "echo 'Caught signal! Exiting..'; cleanup" ERR SIGINT

printf "%s\n" "-- Running tst_mmap-size in orderly"
duration_run1="$(podman run --rm -t -v "$PWD":"$PWD" -w "$PWD" --name orderly centos:stream9 \
                 ./tst_mmap-size $mem_size_mb 2>&1 | sed '1d' | cut -d ' ' -f5)"

printf "%s\n" "-- Running tst_mem-frag in confusion"
podman run -td -v "$PWD":"$PWD" -w "$PWD" --name confusion centos:stream9 ./tst_mem-frag

wait_for_fragmentation

sleep 1
printf "%s\n" "-- Running tst_mmap-size in orderly"
duration_run2="$(podman run --rm -t -v "$PWD":"$PWD" -w "$PWD" --name orderly centos:stream9 \
                 ./tst_mmap-size $mem_size_mb 2>&1 | sed '1d' | cut -d ' ' -f5)"

cleanup

if test -z "$duration_run1" || test -z "$duration_run2"; then
  exit 1
fi

duration_difference=$(( $duration_run1 - $duration_run2 ))
duration_difference=${duration_difference#-} # abs()
duration_rel_error=$(( $duration_run1 / 5 ))

printf "%s %d %s %d\n" "-- Run1: " $duration_run1 " Run2: " $duration_run2
printf "%s %d %s %d\n" "-- Difference: " $duration_difference " Relative error: " $duration_rel_error

if test $duration_difference -lt $duration_rel_error; then
  printf "%s\n" "-- Success"
  exit 0
else
  printf "%s\n" "-- Failure"
  exit 1
fi

