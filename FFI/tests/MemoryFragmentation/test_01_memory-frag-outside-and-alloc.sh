#!/bin/bash

set -eE
cleanup() {
  podman rm -if orderly
  test -n "$job_pid" && kill "$job_pid"
}

mem_test_size() {
  size_total_mb=$(grep "MemAvailable" /proc/meminfo | awk '{ print $2 }')
  echo "$size_total_mb / (2 * 1024) * (1 - 0.1)" | bc
}

mem_size_mb=$(mem_test_size)

chcon -R -t container_file_t .


trap "echo 'Caught signal! Exiting..'; cleanup" ERR SIGINT

printf "%s\n" "-- Running tst_mmap-size in orderly"
duration_run1="$(podman run --rm -t -v "$PWD":"$PWD" -w "$PWD" --name orderly centos:stream9 \
                 ./tst_mmap-size $mem_size_mb 2>&1 | sed '1d' | cut -d ' ' -f5)"


printf "%s\n" "-- Running tst_mem-frag"
./tst_sys_mem-frag &
job_pid=$!
sleep 10

printf "%s\n" "-- Running tst_mmap-size in orderly"
duration_run2="$(podman run --rm -t -v "$PWD":"$PWD" -w "$PWD" --name orderly centos:stream9 \
                 ./tst_mmap-size $mem_size_mb 2>&1 | sed '1d' | cut -d ' ' -f5)"

cleanup

if test -z "$duration_run1" || test -z "$duration_run2"; then
  exit 1
fi

duration_difference=$(( $duration_run1 - $duration_run2 ))
duration_difference=${duration_difference#-} # abs()
duration_rel_error=$(( $duration_run1 / 5 ))

printf "%s %d %s %d\n" "-- Run1: " $duration_run1 " Run2: " $duration_run2
printf "%s %d %s %d\n" "-- Difference: " $duration_difference " Relative error: " $duration_rel_error

if test $duration_difference -lt $duration_rel_error; then
  printf "%s\n" "-- Success"
  exit 0
else
  printf "%s\n" "-- Failure"
  exit 1
fi

