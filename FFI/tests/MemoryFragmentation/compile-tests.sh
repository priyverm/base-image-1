#!/bin/bash

set -eE
trap 'podman rmi --force $image' ERR SIGINT

random() {
  shuf -ern 10 {a..z} | tr -d '\n'
}

patch -p1 -i mem-frag-test_DEBUG.patch
patch -p1 -i mem-frag-test_performance.patch
patch -p1 -i mem-frag-test_totalmem.patch

gcc -o tst_sys_mem-frag mem-frag-test.c -Wall -Werror

image=$(random)

cat <<EOF | podman build -t $image -f -
FROM quay.io/centos/centos:stream9

RUN dnf install -y gcc && dnf clean all
EOF

chcon -R -t container_file_t .

podman run --rm -v $PWD:$PWD -w $PWD --name container $image \
           gcc -o tst_mem-frag mem-frag-test.c -Wall -Werror
podman run --rm -v $PWD:$PWD -w $PWD --name container $image \
           gcc -o tst_mmap-size mmap_size.c -DDEBUG -Wall -Werror

podman rmi --force $image

