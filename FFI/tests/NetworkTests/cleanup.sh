#!/bin/sh

#Kill ping and remove the containers
pkill ping
echo "Cleaning up containers"
echo ">>> Remove container orderly"
podman rm -f orderly
echo ">>> Remove container confusion"
podman rm -f confusion
echo "End Test"
