#!/bin/bash

#Here $timper is allowed percentage delay and $ploss is allowed packet loss
timper=5
ploss=0
echo " "
echo "**********Creating Containers**********"
echo " "
podman run -d --name orderly stream9-stress
podman run -d --name confusion stream9-stress
echo " "
echo "**********Getting IP of Orderly*********"
echo " "
podman inspect orderly | grep IPAddress
var=$(podman inspect orderly | grep IPAddress | tail -1 | awk '{print $NF}' |  sed 's/"//g' | sed 's/,//g')
echo " "
echo "**********Getting IP of Confusion**********"
echo " "
podman inspect confusion | grep IPAddress
var2=$(podman inspect confusion | grep IPAddress | tail -1 | awk '{print $NF}' |  sed 's/"//g' | sed 's/,//g')
echo " "
echo "*****Orderly pings target:dns9.quad9.net 10 times*****"
echo " "
var3=$(podman exec -it orderly sh -c "ping -c 10 dns9.quad9.net" | grep packet| tail -1 | awk '{print $NF}' | sed 's/[ms]//g')
sleep 2
echo -e "\e[1;35m Time taken to ping in ideal condition is $var3 ms \e[0m"
echo "*****Confusion container sends large unstoppable packets to orderly, while orderly pings target :dns9.quad9.net 10 times*****"
echo " "
podman exec -it confusion sh -c "ping -s 1600 $var" &
sleep 5
var4=$(podman exec -it orderly sh -c "ping -c 10 dns9.quad9.net" | grep packet | tail -1 | awk '{print $NF}'| sed 's/[ms]//g')
echo ""
sleep 2
echo  -e "\e[1;35m Time taken to ping target in while in a heavy traffic is $var4 ms \e[0m"
var5=$(awk -v t1="$var3" -v t2="$var4" 'BEGIN{printf "%.0f", (t2-t1)/t1 * 100}')
sleep 1
echo ""
echo -e "\e[1;32m Percentage increase in time taken is $var5 percent \e[0m"
echo ""
if [[ $var5 -gt $timper ]]
then
  echo -e "\e[1;31m Interference is present - Delay in pinging target when under stress \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No Delay in pinging target under stress condition created by Confusion Container \e[0m"
fi
echo ""
echo -e "\e[1;35m Looking for ICMP errors and Packet loss"
./watch-it.sh &
var6=$(podman exec -it orderly sh -c "ping -c 20 dns9.quad9.net"  | grep packet | tail -1 | awk '{print $4}')
echo -e "\e[1;32m Packet loss is $var6 percent  \e[0m"
if [[ $var6 -gt $ploss ]]
then
  echo -e "\e[1;31m Interfernce is present - presence of packet loss \e[0m"
  exit 1
else
        echo -e "\e[1;32m Interference is not present - No packet loss \e[0m"
fi
./cleanup.sh
