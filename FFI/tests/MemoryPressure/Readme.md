## This Test Suite include 4 tests:

Setup for which includes installing podman, pulling the image and then build the image using relevant epel and stress-ng installation.

1. cpu-mem-stress01:
Two containers running in a vm :
Orderly(Default memory container): `stress-ng --matrix 0 -t 1m` -> using stress-ng matrix exercises the CPU floating point operations as well as memory and processor data cache.
Confusion (Less memory container): `stress-ng --tmpfs 50 --timeout 50` ->Start  50 workers that create a temporary file on an available tmpfs file system and  perform various file based mmap operations.

2. mempressure_both_container:
Two containers running in a vm :
On both containers : `stress-ng --brk 2 --stack 2 --bigheap 2 -t 60s` ->Forcing memory pressure (The --brk (expand heap break point), --stack (expand stack), --bigheap stressors try to rapidly consume memory. The kernel will eventually kill these using the Out Of Memory (OOM) killer, however, stress-ng will respawn the processes to keep the kernel busy. On a system with swap enabled the swap device will be heavily exercised.kept swap = 0).

3. cpu-mem-stress02:
Two containers running in a vm :
On both containers :`stress-ng --cpu 4 --vm 2 --vm-bytes 7G --timeout 60s` -> Exhausts memory.

4. mempressure_one_container:
Two containers running in a vm :
Orderly(Default memory container): `stress-ng --vecmath 1 -t 50s` -> performs vecmath operations
Confusion (Less memory container): `stress-ng --vm 2 --vm-bytes 2G --mmap 2 --mmap-bytes 2G –page-in --timeout 60s` ->When under memory pressure, the kernel will start writing pages out to swap. By checking which pages in a memory mapping are not resident in memory and touching them we can force them back into memory, causing the VM system to be heavily exercised. The --page-in option enables this mode for the bigheap, mmap and vm stressors.

## The metrics for these tests are collected using :
1. `podman ps --all` for listing containers.
2. `mpstat -P ALL 1 45`  for CPU utilization.(yum instal sysstat needed).
3. `vmstat -S m` for memory statistic (The memory statistic will be displayed in MB(megabyte) format- yum install sysstat needed).
4. `free -t -h -w` for shared Resources.

