#!/bin/bash
#This creates the limited memory or cpu container as per the choice mentioned in TYPE
TYPE="${1:-memory}"
declare -a CONTAINER_LIMITS

case "$TYPE" in
    "cpu")
        echo "Start test: limit CPU"
        CONTAINER_LIMITS=(--cpu-shares 512 --cpuset-cpus 0)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"

    "memory"
        echo "Start test: limit memory"
        CONTAINER_LIMITS=(--memory 512m)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"
        ;;
    *)
        ;;
esac

#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream9-stress

#Creates the container with Limited memory
echo "Create Confusion container"
podman run -d "${CONTAINER_LIMITS[@]}" --name confusion --oom-score-adj=-1000 stream9-stress

#Performance.sh here starts collecting metrics in vm
./performance.sh &

#Run stress-ng in both containers Forcing memory pressure (The --brk (expand heap break point), --stack (expand stack), --bigheap stressors try to rapidly consume memory. The kernel will eventually kill these using the Out Of Memory (OOM) killer, however, stress-ng will respawn the processes to keep the kernel busy. On a system with swap enabled the swap device will be heavily exercised.kept swap = 0)
echo "Run some interferences"
podman exec -it confusion sh -c "stress-ng --brk 2 --stack 2 --bigheap 2 -t 50s" &
podman exec -it orderly sh -c "stress-ng --brk 2 --stack 2 --bigheap 2 -t 50s"

#Remove the containers
sleep 5
./cleanup.sh
