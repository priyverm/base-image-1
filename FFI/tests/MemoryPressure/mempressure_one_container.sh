#!/bin/bash
#This creates the limited memory or cpu container as per the choice mentioned in TYPE
TYPE="${1:-memory}"
declare -a CONTAINER_LIMITS

case "$TYPE" in
    "cpu")
        echo "Start test: limit CPU"
        CONTAINER_LIMITS=(--cpu-shares 512 --cpuset-cpus 0)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"

    "memory"
        echo "Start test: limit memory"
        CONTAINER_LIMITS=(--memory 512m)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"
        ;;
    *)
        ;;
esac

#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream9-stress

#Creates the container with Limited memory
echo "Create Confusion container"
podman run -d "${CONTAINER_LIMITS[@]}" --name confusion --oom-score-adj=-1000 stream9-stress

echo "watching orderly"
podman exec -it orderly sh -c  watch-it &

#Run stress-ng in both containers Exhaust memory and cpu of container with Less memory: When under memory pressure, the kernel will start writing pages out to swap. By checking which pages in a memory mapping are not resident in memory and touching them we can force them back into memory, causing the VM system to be heavily exercised. The --page-in option enables this mode for the bigheap, mmap and vm stressors. while container with default memory performs vec math operations.
echo "Run some interferences"
podman exec -it confusion sh -c "stress-ng --vm 2 --vm-bytes 2G --mmap 2 --mmap-bytes 2G –page-in --timeout 50" &
podman exec -it orderly sh -c "stress-ng --vecmath 1 -t 50s"

#Remove the containers
sleep 5
./cleanup.sh
