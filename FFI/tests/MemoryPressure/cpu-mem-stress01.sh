#!/bin/bash
#This creates the limited memory or cpu container as per the choice mentioned in TYPE
TYPE="${1:-memory}"
declare -a CONTAINER_LIMITS

case "$TYPE" in
    "cpu")
        echo "Start test: limit CPU"
        CONTAINER_LIMITS=(--cpu-shares 512 --cpuset-cpus 0)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"

    "memory"
        echo "Start test: limit memory"
        CONTAINER_LIMITS=(--memory 512m)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"
        ;;
    *)
        ;;
esac
#Create the container with Default memory
echo "Create Orderly container"
podman run -d --name orderly stream9-stress

#Creates the container with Limited memory
echo "Create Confusion container"
podman run -d "${CONTAINER_LIMITS[@]}" --name confusion  --oom-score-adj=-1000 stream9-stress

#Performance.sh here starts collecting metrics in vm
./performance.sh &

#Run stress-ng in both containers
echo "Run some interferences"
podman exec -it confusion sh -c "stress-ng --tmpfs 50 --timeout 50" & # Start 50 workers that create a temporary file on an available tmpfs file system and  perform various file based mmap operation #
podman exec -it orderly sh -c "stress-ng --matrix 0 -t 50s" # stress-ng matrix exercises the CPU floating point operations as well as memory and processor data cache #

#Remove the containers
sleep 5
./cleanup.sh
